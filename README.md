
**usblogger** is a keylogger for embedded devices like the Raspberry Pi. It 
was developed for logging keyboard events sent by an arduino usb keyboard 
interface. Once installed it logs the keyboard events of a configured keyboard 
device as soon as plugged in and makes them available via Web interface. The 
code is written using Javascript, JQuery, Bootstrap, Python and Shell scripting.

