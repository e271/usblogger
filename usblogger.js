
//  Copyright (C) 2016, Gabriel Klawitter
//  This file is part of usblogger.
//
//  usblogger is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Foobar is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.





function update() {
    // run my site

	$.get( "usblogger.py", function( data ) {
        $("#usbevents").html(data.logdata);
        $("#statusline").html(data.statusline);
	}, 'json');
}

$(function () {
    update();
    // var mainTimer = setInterval(update, 20000);

});


// vim: ai ts=4 sw=4 sts=4 expandtab
