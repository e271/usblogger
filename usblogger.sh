#!/bin/sh

#   Copyright (C) 2016, Gabriel Klawitter
#   This file is part of usblogger.
#
#   usblogger is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Foobar is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.


# this file is supposed to be invoked by udev as soon as a arduino keyboard is 
# plugged in.


LOGPATH="/var/log/usblogger"
LOGFILE="${LOGPATH}/log.$(date '+%Y-%m-%d.%H:%M:%S')"
LOGGRP="www-data" 
LOGPERM="640"
EVTEST="/usr/bin/evtest"



log () {
    /usr/bin/logger -t USBLOGGER $@
}

# device id of the original leonardo board
# /dev/input/by-id/usb-Arduino_LLC_Arduino_Leonardo-if02-event-mouse

sleep 2

# DEVNAME shall be given by udev
if [ -z "${DEVNAME}" ]
then
    DEV="$(ls -1 /dev/input/by-id/usb-Arduino_*-event-mouse | head -n1)"
else
    DEV="${DEVNAME}"
fi

if [ -z "${DEV}" ]
then
    log "no arduino event mouse device found - nothing to log here"
    exit 0
fi

if ! [ -x "${EVTEST}" ]
then
    log "evtest package not installed"
    exit 0
fi


if pgrep -f "evtest.*usb-Arduino_LLC_Arduino_Leonardo-if02-event-mouse" > /dev/null
then
    log "evtest on ${DEV} already running"
    exit 0
fi


mkdir -p ${LOGPATH}
touch ${LOGFILE}
chgrp ${LOGGRP} ${LOGFILE}
chmod ${LOGPERM} ${LOGFILE}
env > ${LOGFILE}


# now hand over to the evtest logger
log "starting evtest on ${DEV} logfile ${LOGFILE}"

# no need to fork in background since we're started by systemd now
${EVTEST} ${DEV} >> ${LOGFILE} 2>&1







# vim: ai ts=4 sw=4 sts=4 expandtab
