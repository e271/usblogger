#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#   Copyright (C) 2016, Gabriel Klawitter
#   This file is part of usblogger.
#
#   usblogger is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Foobar is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.



LOGPATH='/var/log/usblogger'


# print('Content-Type: text/html;charset=utf-8')
print('Content-Type: application/json')
print()

# import cgi
# import cgitb
import os
import glob
from time import strftime,localtime,time
from json import JSONEncoder

# fs = cgi.FieldStorage()

try:
    # LOGFILE="/mnt/evtest/event.evtest"
    LOGFILE = max(glob.iglob( LOGPATH + '/log.*'), key=os.path.getctime)
    
    f = open(LOGFILE, 'r')

    statusline = '%s: ' % LOGFILE
    logdata = ''

    numEvents = 0
    timeRange = ''
    timeFirst = ''
    timeLast = ''
    
    for line in f:
        if not ', type 1 (EV_KEY),' in line:
            continue
    
        numEvents += 1
        k = line.split(',')
        t = int(float(k[0].split(' ')[2]))
        if not timeFirst:
            timeFirst = t
        timeLast = t
        lt = strftime('%Y-%m-%d %H:%M:%S', localtime(t))
        w = k[2]
        v = k[3].split(' ')[2]
        # print("<tr><td>%s</td><td>%s</td><td>%s</td></tr>" % (lt, w, v))
        logdata += "<tr><td>%s</td><td>%s</td><td>%s</td></tr>\n" % (lt, w, v)
    
    f.close()

    timeRange = timeLast - timeFirst
    timeString = ''
    ratioString = '%.3f events/second' % (numEvents / timeRange)

    if timeRange > 3600:
        hours = int(timeRange/3600)
        timeString += '%sh ' % hours
        timeRange -= hours * 3600
    if timeRange > 180:
        minutes = int(timeRange/60)
        timeString += '%sm ' % minutes
        timeRange -= minutes * 60

    timeString += '%ss' % timeRange

    statusline += '%s events in %s seconds i.e. %s ' \
            % (numEvents, timeString, ratioString)

    response = { 'statusline': statusline, 'logdata': logdata }
    print(JSONEncoder().encode(response))
except:
    response = { 'statusline': 'something went wrong',
            'logdata': '<tr><td></td><td>no logfile found or unknown format</td><td></td></tr>\n' }
    print(JSONEncoder().encode(response))
    raise


# vim: ai ts=4 sw=4 sts=4 expandtab
